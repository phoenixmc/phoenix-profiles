package com.infermc.phoenix;

import com.infermc.phoenix.achievements.AchievementManager;
import com.infermc.phoenix.achievements.PhoenixAchievement;
import com.infermc.phoenix.listeners.*;
import com.infermc.phoenix.profiles.Profile;
import com.infermc.phoenix.profiles.ProfileManager;
import com.infermc.phoenix.profiles.ProfileUtil;
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Achievement;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.*;
import java.util.Date;

public class PhoenixProfiles extends JavaPlugin implements Listener {
    private Economy eco;
    private Permission perms;
    protected Double multiplier = .0;

    public ProfileUtil util = new ProfileUtil();

    // Data management
    private DatabaseManager databaseManager;
    private ProfileManager profileManager;
    private AchievementManager achievementManager;

    // Whitelisting
    private boolean whitelisted = false;

    // Timers
    int heartbeatTask = -1;

    public void onEnable() {
        // Put the server in whitelist mode while we start!
        whitelisted = getServer().hasWhitelist();
        getServer().setWhitelist(true);

        // Save a copy of our configuration
        saveDefaultConfig();

        getServer().getPluginManager().registerEvents(this,this);

        // Chat Listener
        getServer().getPluginManager().registerEvents(new ChatListener(this),this);
        // Player Events from the server
        getServer().getPluginManager().registerEvents(new PlayerListener(this),this);

        if (!setupEconomy()) getLogger().warning("Error setting up economy api!");
        if (!setupPermissions()) getLogger().warning("Error setting up permissions api!");

        databaseManager = new DatabaseManager(this,getConfig().getString("database"));

        getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable() {
            @Override
            public void run() {
                delayedLoad();
            }
        },20L);
    }
    // Load stuff a few seconds after everything else
    public void delayedLoad() {
        getLogger().info("Running delayed load tasks.");
        // If we support dungeons
        if (getServer().getPluginManager().isPluginEnabled("PhoenixRPG-Dungeon")) {
            getServer().getPluginManager().registerEvents(new DungeonListener(this),this);
        } else {
            getLogger().warning("Unable to load dungeon support! PhoenixRPG-Dungeon isn't loaded!");
        }

        // Jobs Listener
        if (getServer().getPluginManager().isPluginEnabled("Jobs")) {
            getServer().getPluginManager().registerEvents(new JobsListener(this),this);
            getLogger().info("Registered Jobs Listener");
        } else {
            getLogger().info("Unable to load Jobs support! Jobs isn't loaded!");
        }

        // mcMMO Listener
        if (getServer().getPluginManager().isPluginEnabled("mcMMO")) {
            getServer().getPluginManager().registerEvents(new McmmoListener(this),this);
            getLogger().info("Registered mcMMO Listener");
        } else {
            getLogger().info("Unable to load mcMMO support! mcMMO isn't loaded!");
        }

        // MobArena Listener
        if (getServer().getPluginManager().isPluginEnabled("MobArena")) {
            getServer().getPluginManager().registerEvents(new MobArenaListener(this),this);
            getLogger().info("Registered MobArena Listener");
        }

        // Votifier Listener
        if (getServer().getPluginManager().isPluginEnabled("Votifier")) {
            getServer().getPluginManager().registerEvents(new VoteListener(this),this);
            getLogger().info("Registered Votifier Listener");
        }

        profileManager = new ProfileManager(this,databaseManager,eco);
        achievementManager = new AchievementManager(this,databaseManager);

        // Update player stuff.
        for (Player p : getServer().getOnlinePlayers()) {
            updatePlayer(p);
        }
        // Every 5mins
        heartbeatTask = getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {
            @Override
            public void run() {
                updateStats();
                for (Player p :getServer().getOnlinePlayers()) {
                    Profile profile = profileManager.getProfile(p);
                    if (profile == null) profile = profileManager.createProfile(p);
                    int curStamp = (int) Math.floor(new java.util.Date().getTime()/1000);
                    profile.setLastSeen(curStamp);
                }
            }
        },2400L,2400L);

        // Should be ready
        getLogger().info("Assuming plugin is ready. Restoring whitelist setting!");
        getServer().setWhitelist(whitelisted);
    }

    @Override
    public void onDisable() {
        if (heartbeatTask >= 0) {
            getServer().getScheduler().cancelTask(heartbeatTask);
        }
    }

    // Inserts current stats into MySQL
    private void updateStats() {
        Connection conn = getDatabaseManager().getConnection();
        if (conn == null) {
            getLogger().info("Not connected to database, skipping stat heartbeat.");
            return;
        }
        getLogger().info("Statistic Heartbeat");
        try {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO `site_server_stats` (server,player_count,tps,stamp) VALUES(?,?,?,?);");
            stmt.setInt(1,getConfig().getInt("server_id",-1));
            stmt.setInt(2,getServer().getOnlinePlayers().size());
            stmt.setDouble(3,getServer().getTPS()[0]);
            stmt.setInt(4,util.getTime());
            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @EventHandler
    public void onConnect(PlayerJoinEvent ev) {
        updatePlayer(ev.getPlayer());
    }

    public void updatePlayer(Player p) {
        Connection conn = databaseManager.getConnection();
        if (conn == null) {
            getLogger().warning("Error updating player! Database not connected :(");
            return;
        }
        Profile profile = profileManager.getProfile(p);
        if (profile == null) profile = profileManager.createProfile(p);
        int curStamp = (int) Math.floor(new java.util.Date().getTime()/1000);
        profile.setLastSeen(curStamp);
        profile.setUsername(p.getName());
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent ev) {
        if (databaseManager.getConnection() == null) {
            getLogger().warning("Error updating player! Database not connected :(");
            return;
        }
        Profile profile = profileManager.getProfile(ev.getPlayer());
        if (profile == null) return;
        profile.getMeta().set("last_quit", String.valueOf( (int) Math.floor(new Date().getTime()/1000) )  );
    }

    public ProfileManager getProfileManager() {
        return this.profileManager;
    }
    public AchievementManager getAchievementManager() { return this.achievementManager; }
    public DatabaseManager getDatabaseManager() { return this.databaseManager; }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = null;
        if (sender instanceof Player) player = (Player) sender;

        String cmd = command.getName();
        if (cmd.equalsIgnoreCase("profile")) {
            Profile profile;
            if (player != null) {
                if (args.length == 0) {
                    profile = profileManager.getProfile(player);
                } else {
                    String name = args[0];
                    profile = profileManager.getProfile(name);
                }
            } else {
                if (args.length == 0) {
                    sender.sendMessage("Usage: /profile [username]");
                    return true;
                } else {
                    String name = args[0];
                    profile = profileManager.getProfile(name);
                }
            }
            if (profile != null) {
                sender.sendMessage(ChatColor.GREEN + "=== " + profile.getUsername() + " ===");
                //sender.sendMessage(ChatColor.AQUA + " Title: " + lbl);
                sender.sendMessage(ChatColor.AQUA + " Level: " + profile.getLevel());

                Integer requiredExp = this.util.getExperience(profile.getLevel()+1);

                if (profile.getLegendLevel() > 0) {
                    requiredExp = this.util.getExperience(profile.getLegendLevel()+51);
                    sender.sendMessage(ChatColor.AQUA + " Legendary Level: " + profile.getLegendLevel());
                }
                if (sender.equals(profile.getPlayer())) {
                    sender.sendMessage(ChatColor.AQUA + " Experience:");
                    sender.sendMessage(ChatColor.AQUA + "   Current: " + profile.getExperience());
                    sender.sendMessage(ChatColor.AQUA + "   Next Level: " + requiredExp);
                    sender.sendMessage(ChatColor.AQUA + "   Difference: " + (requiredExp - profile.getExperience()));

                    Integer ruinsFound = profile.getMeta().getInt("ruins_found");
                    if (ruinsFound == null) ruinsFound = 0;
                    sender.sendMessage(ChatColor.AQUA + " Ruins Found: "+ruinsFound);

                } else {
                    sender.sendMessage(ChatColor.AQUA + " Experience: "+profile.getExperience());
                }
                sender.sendMessage(ChatColor.AQUA + " Achievements: " + profile.getAchievements().size());
            } else {
                sender.sendMessage(ChatColor.DARK_RED+"Error! Cannot find player!");
            }
            return true;
        } else if (cmd.equalsIgnoreCase("pp")) {
            // Profile management.
            if (args.length <= 0) {
                sender.sendMessage("Syntax: /pp [exp|level|achievement]");
                return true;
            } else {
                if (args[0].equalsIgnoreCase("exp")) {
                    if (args.length >= 2) {
                        if (args[1].equalsIgnoreCase("set")) {
                            if (sender.hasPermission("phonenixprofiles.exp.set")) {
                                if (args.length >= 4) {
                                    String target = args[2];
                                    String amountStr = args[3];
                                    Profile profile = getProfileManager().getProfile(target);
                                    if (profile == null) {
                                        sender.sendMessage(ChatColor.RED+"Error! Cannot find player!");
                                        return true;
                                    } else {
                                        try {
                                            Integer amount = Integer.valueOf(amountStr);
                                            profile.setExperience(amount);
                                            sender.sendMessage(ChatColor.GREEN + "Experience for " + profile.getUsername() + " set to " + amount);
                                            return true;
                                        } catch (NumberFormatException e) {
                                            sender.sendMessage(ChatColor.RED+"Experience must be a number!");
                                            return true;
                                        }
                                    }
                                } else {
                                    sender.sendMessage("Syntax: /pp exp set <username> <amount>");
                                    return true;
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED+"Permission Denied!");
                                return true;
                            }
                        } else if (args[1].equalsIgnoreCase("give")) {
                            if (sender.hasPermission("phonenixprofiles.exp.give")) {
                                if (args.length >= 4) {
                                    String target = args[2];
                                    String amountStr = args[3];
                                    Profile profile = getProfileManager().getProfile(target);
                                    if (profile == null) {
                                        sender.sendMessage(ChatColor.RED + "Error! Cannot find player!");
                                        return true;
                                    } else {
                                        try {
                                            Integer amount = Integer.valueOf(amountStr);
                                            profile.giveExperience(amount);
                                            sender.sendMessage(ChatColor.GREEN + "Given " + amount + " experience to " + profile.getUsername());
                                            return true;
                                        } catch (NumberFormatException e) {
                                            sender.sendMessage(ChatColor.RED+"Experience must be a number!");
                                            return true;
                                        }
                                    }
                                } else {
                                    sender.sendMessage("Syntax: /pp exp give <username> <amount>");
                                    return true;
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED+"Permission Denied!");
                                return true;
                            }
                        } else if (args[1].equalsIgnoreCase("take")) {
                            if (sender.hasPermission("phonenixprofiles.exp.take")) {
                                if (args.length >= 4) {
                                    String target = args[2];
                                    String amountStr = args[3];
                                    Profile profile = getProfileManager().getProfile(target);
                                    if (profile == null) {
                                        sender.sendMessage(ChatColor.RED + "Error! Cannot find player!");
                                        return true;
                                    } else {
                                        try {
                                            Integer amount = Integer.valueOf(amountStr);
                                            profile.takeExperience(amount);
                                            sender.sendMessage(ChatColor.GREEN + "Taken " + amount + " experience away from " + profile.getUsername());
                                            return true;
                                        } catch (NumberFormatException e) {
                                            sender.sendMessage(ChatColor.RED+"Experience must be a number!");
                                            return true;
                                        }
                                    }
                                } else {
                                    sender.sendMessage("Syntax: /pp exp take <username> <amount>");
                                    return true;
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED+"Permission Denied!");
                            }
                        } else if (args[1].equalsIgnoreCase("get")) {
                            if (sender.hasPermission("phonenixprofiles.exp.get")) {
                                if (args.length >= 3) {
                                    String target = args[2];
                                    Profile profile = getProfileManager().getProfile(target);
                                    if (profile == null) {
                                        sender.sendMessage(ChatColor.RED + "Error! Cannot find player!");
                                        return true;
                                    } else {
                                        sender.sendMessage(ChatColor.GREEN + profile.getUsername() + " currently has " + profile.getExperience() + " experience.");
                                        return true;
                                    }
                                } else {
                                    sender.sendMessage("Syntax: /pp exp get <username>");
                                    return true;
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED+"Permission Denied!");
                                return true;
                            }
                        } else {
                            sender.sendMessage("Syntax: /pp exp [set|give|take|get] [username] [amount]");
                            return true;
                        }
                    } else {
                        sender.sendMessage("Syntax: /pp exp [set|give|take|get] [username] [amount]");
                        return true;
                    }
                } else if (args[0].equalsIgnoreCase("level")) {
                    if (args.length >= 2) {
                        if (args[1].equalsIgnoreCase("set")) {
                            if (sender.hasPermission("phoenixprofiles.level.set")) {
                                if (args.length >= 4) {
                                    String target = args[2];
                                    String amountStr = args[3];
                                    boolean isLegend = false;
                                    Profile profile = getProfileManager().getProfile(target);
                                    if (args.length >= 5) {
                                        if (args[4].equalsIgnoreCase("--legend")) isLegend = true;
                                    }
                                    if (profile == null) {
                                        sender.sendMessage(ChatColor.RED + "Error! Cannot find player!");
                                        return true;
                                    } else {
                                        try {
                                            Integer amount = Integer.valueOf(amountStr);
                                            if (isLegend) {
                                                amount += 50;
                                            } else {
                                                if (amount > 50) amount = 50;
                                            }
                                            Integer exp = util.getExperience(amount);
                                            profile.setExperience(exp);
                                            if (isLegend) {
                                                sender.sendMessage(ChatColor.GREEN + "Legend level set to " + (amount-50) + " for " + profile.getUsername());
                                            } else {
                                                sender.sendMessage(ChatColor.GREEN + "Level set to " + amount + " for " + profile.getUsername());
                                            }
                                            return true;
                                        } catch (NumberFormatException e) {
                                            sender.sendMessage(ChatColor.RED + "Level must be a number!");
                                            return true;
                                        }
                                    }
                                } else {
                                    sender.sendMessage("Syntax: /pp level set <username> <level> --legend");
                                    return true;
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED+"Permission Denied!");
                                return true;
                            }
                        } else if (args[1].equalsIgnoreCase("get")){
                            if (sender.hasPermission("phoenixprofiles.level.get")) {
                                if (args.length >= 3) {
                                    String target = args[2];
                                    Profile profile = getProfileManager().getProfile(target);
                                    if (profile == null) {
                                        sender.sendMessage(ChatColor.RED+"Error! Cannot find player!");
                                        return true;
                                    } else {
                                        sender.sendMessage(ChatColor.GREEN + profile.getUsername() + " is level " + profile.getLevel() + " and legend level " + profile.getLegendLevel());
                                        return true;
                                    }
                                } else {
                                    sender.sendMessage("Syntax: /pp level get <username>");
                                    return true;
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED+"Permission Denied!");
                                return true;
                            }
                        } else {
                            sender.sendMessage("Syntax: /pp level [set|get] [username] [level] [--legend]");
                            return true;
                        }
                    } else {
                        sender.sendMessage("Syntax: /pp level [set|get] [username] [level] [--legend]");
                        return true;
                    }
                } else if (args[0].equalsIgnoreCase("achievement")) {
                    if (args.length >= 3) {
                        if (args[1].equalsIgnoreCase("get")) {
                            if (sender.hasPermission("phoenixprofiles.achievement.get")) {
                                String achv = args[2];
                                PhoenixAchievement achievement = getAchievementManager().getAchievement(achv);
                                if (achievement == null) {
                                    sender.sendMessage(ChatColor.RED+"Error! Cannot find achievement!");
                                } else {
                                    sender.sendMessage(ChatColor.AQUA+"=== "+achievement.getSlug()+" ===");
                                    sender.sendMessage(ChatColor.AQUA+""+ChatColor.BOLD+achievement.getTitle());
                                    sender.sendMessage(ChatColor.AQUA+achievement.getDescription());
                                    sender.sendMessage(ChatColor.AQUA+"+"+achievement.getExpgain()+" EXP");
                                }
                                return true;
                            } else {
                                sender.sendMessage(ChatColor.RED+"Permission Denied!");
                                return true;
                            }
                        } else if (args[1].equalsIgnoreCase("has")) {
                            if (sender.hasPermission("phoenixprofiles.achievement.has")) {
                                if (args.length >= 4) {
                                    String target = args[2];
                                    String achv = args[3];

                                    Profile profile = getProfileManager().getProfile(target);
                                    if (profile == null) {
                                        sender.sendMessage(ChatColor.RED+"Error! Cannot find player!");
                                        return true;
                                    }
                                    PhoenixAchievement achievement = getAchievementManager().getAchievement(achv);
                                    if (achievement == null) {
                                        sender.sendMessage(ChatColor.RED+"Error! Cannot find achievement!");
                                        return true;
                                    }
                                    if (profile.hasAchievement(achievement)) {
                                        sender.sendMessage(ChatColor.GREEN+profile.getUsername()+" has the achievement "+achievement.getTitle()+" ("+achievement.getSlug()+")");
                                    } else {
                                        sender.sendMessage(ChatColor.RED+profile.getUsername()+" does not have the achievement "+achievement.getTitle()+" ("+achievement.getSlug()+")");
                                    }
                                    return true;
                                } else {
                                    sender.sendMessage("Syntax: /pp achievement has [username] [achievement]");
                                    return true;
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED+"Permission Denied!");
                                return true;
                            }
                        } else if (args[1].equalsIgnoreCase("give")){
                            if (sender.hasPermission("phoenixprofiles.achievement.give")) {
                                if (args.length >= 4) {
                                    String target = args[2];
                                    String achv = args[3];

                                    Profile profile = getProfileManager().getProfile(target);
                                    if (profile == null) {
                                        sender.sendMessage(ChatColor.RED+"Error! Cannot find player!");
                                        return true;
                                    }
                                    PhoenixAchievement achievement = getAchievementManager().getAchievement(achv);
                                    if (achievement == null) {
                                        sender.sendMessage(ChatColor.RED+"Error! Cannot find achievement!");
                                        return true;
                                    }

                                    if (profile.hasAchievement(achievement)) {
                                        sender.sendMessage(ChatColor.RED+"Error! "+profile.getUsername()+" already has that achievement!");
                                    } else {
                                        profile.giveAchievement(achievement);
                                        sender.sendMessage(ChatColor.GREEN+"Success! Achievement given to "+profile.getUsername());
                                    }
                                    return true;
                                } else {
                                    sender.sendMessage("Syntax: /pp achievement give [username] [achievement]");
                                    return true;
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED+"Permission Denied!");
                                return true;
                            }
                        } else {
                            sender.sendMessage("Syntax: /pp achievement [get|has|give] [username] [achievement]");
                            return true;
                        }
                    } else {
                        sender.sendMessage("Syntax: /pp achievement [get|has|give] [username] [achievement]");
                        return true;
                    }
                } else {
                    sender.sendMessage("Syntax: /pp [exp|level|achievement]");
                    return true;
                }
            }
        } else if (cmd.equalsIgnoreCase("phoenix")) {
            if (args.length >= 1) {
                if (args[0].equalsIgnoreCase("verify")) {
                    if (args.length >= 2) {
                        String code = args[1];
                        if (player != null) {
                            Profile profile = getProfileManager().getProfile(player);
                            if (profile != null) {
                                boolean res = getProfileManager().verifyProfile(profile, code);
                                if (res) {
                                    sender.sendMessage(ChatColor.GREEN + "Verified successfully!");
                                    if (perms != null) {
                                        String group = perms.getPrimaryGroup(player);
                                        if (group.equalsIgnoreCase("peasant")) {
                                            perms.playerAddGroup(player,"Citizen");
                                            sender.sendMessage(ChatColor.GREEN+"You've automatically been upgraded to Citizen for verifying!");
                                        }
                                    }
                                } else {
                                    sender.sendMessage(ChatColor.RED + "Unable to verify! Your code is invalid or has already been used!");
                                }
                            } else {
                                sender.sendMessage(ChatColor.RED + "Error retrieving your profile!");
                            }
                            return true;
                        } else {
                            sender.sendMessage("This is a player only command!");
                            return true;
                        }
                    } else {
                        sender.sendMessage("Syntax: /profile verify [code]");
                        return true;
                    }
                } else {
                    sender.sendMessage("Syntax: /profile verify");
                    return true;
                }
            } else {
                sender.sendMessage("Syntax: /profile verify");
                return true;
            }
        }
        return false;
    }

    public Integer addMultiplier(Integer exp) {
        exp += (int) Math.floor(exp * multiplier);
        return exp;
    }
    public Double getMultiplier() {
        return this.multiplier;
    }
    public void setMultiplier(Double m) {
        this.multiplier = m;
    }

    @Deprecated
    public String getLevelLabel(Integer level) {
        Connection conn = databaseManager.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM `site_exp_level` WHERE min_level <= ? ORDER BY min_level DESC LIMIT 1");
            stmt.setInt(1,level);

            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                rs.first();

                String label = rs.getString("title");

                rs.close();
                return label;
            }

            rs.close();
            return null;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        eco = rsp.getProvider();
        return eco != null;
    }
    private boolean setupPermissions() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        if (rsp == null) {
            return false;
        }
        perms = rsp.getProvider();
        return perms != null;
    }
}
