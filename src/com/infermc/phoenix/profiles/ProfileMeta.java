package com.infermc.phoenix.profiles;

import java.util.HashMap;

public class ProfileMeta {
    private Profile profile;
    private HashMap<String, String> metadata = new HashMap<String, String>();

    public ProfileMeta(Profile p, HashMap<String,String> m) {
        this.profile = p;
        this.metadata = m;
    }
    public ProfileMeta(Profile p) {
        this.profile = p;
    }

    // Getters
    public String getString(String key) {
        if (this.metadata.containsKey(key)) {
            return this.metadata.get(key);
        }
        return null;
    }
    public Integer getInt(String key) {
        String data = getString(key);
        if (data==null) return null;
        return Integer.valueOf(data);
    }
    public Double getDouble(String key) {
        String data = getString(key);
        if (data==null) return null;
        return Double.valueOf(data);
    }

    // Private getter
    protected HashMap<String,String> getContents() {
        return this.metadata;
    }

    // Setters
    public void set(String key,String val) {
        this.metadata.put(key,val);
        this.update();
    }

    // Update the db (Replace with a better method some day soon
    private void update() {
        this.profile.manager.saveProfile(this.profile);
    }
}
