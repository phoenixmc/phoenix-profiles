package com.infermc.phoenix.profiles;

import org.bukkit.ChatColor;

import java.util.Date;

public class ProfileUtil {
    // Normal Level, capped at 50
    public Integer getLevel(Integer exp) {
        Integer level = (int) Math.floor( .017 * Math.sqrt(exp));
        if (level > 50) level = 50;
        return level;
    }

    // Calculates the players level and removes 50 (Normal cap)
    // Normalising the value to 0 if it ends up being below.
    public Integer getLegendLevel(Integer exp) {
        if (exp < 0) exp = 0;

        Integer level = (int) Math.floor( .017 * Math.sqrt(exp))-50;

        if (level < 0) level = 0;
        if (level > 1000) level = 1000;

        return level;
    }

    public Integer getExperience(Integer level) {
        // (2 / 0.017)^2
        Integer exp = (int) Math.ceil( Math.pow( level / 0.017,2) );
        return exp;
    }

    public String colourText(String text) {
        return ChatColor.translateAlternateColorCodes('&',text);
    }
    public String stripColour(String text) {
        text = colourText(text);
        return ChatColor.stripColor(text);
    }

    public int getTime() {
        return (int) Math.floor( new Date().getTime() / 1000 );
    }
}
