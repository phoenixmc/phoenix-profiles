package com.infermc.phoenix.profiles;

import com.infermc.phoenix.DatabaseManager;
import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.achievements.PhoenixAchievement;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class ProfileManager {
    protected PhoenixProfiles parent;
    protected ProfileUtil util;
    protected DatabaseManager databaseManager;
    protected Economy economy;

    public ProfileManager(PhoenixProfiles parent, DatabaseManager connMgr,Economy eco) {
        this.parent = parent;
        this.util = parent.util;
        this.databaseManager = connMgr;
        this.economy = eco;
    }

    public boolean verifyProfile(Profile p, String code) {
        Connection conn = databaseManager.getConnection();
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM `site_characters` WHERE player_id=? AND code=? AND verified=0");
            stmt.setString(1,p.getUniqueId().toString());
            stmt.setString(2,code);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                rs.first();
                Integer id = rs.getInt("id");
                rs.close();
                stmt = conn.prepareStatement("UPDATE `site_characters` SET verified=1 WHERE id=?");
                stmt.setInt(1,id);
                stmt.execute();
                return true;
            } else {
                rs.close();
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public Profile getProfile(Player p) {
        return getProfile(p.getUniqueId());
    }
    public Profile getProfile(OfflinePlayer p) {
        return getProfile(p.getUniqueId());
    }
    public Profile getProfile(String uname) {
        Connection conn = databaseManager.getConnection();
        if (conn == null) return null;
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM `site_players` WHERE username=?");
            stmt.setString(1,uname);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                rs.first();
                UUID uuid = UUID.fromString(rs.getString("uuid"));
                rs.close();
                return getProfile(uuid);
            } else {
                rs.close();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public Profile getProfile(UUID u) {
        Connection conn = databaseManager.getConnection();
        if (conn == null) return null;
        try {
            PreparedStatement stmt = conn.prepareStatement("SELECT * FROM `site_players` WHERE uuid=?");
            stmt.setString(1,u.toString());
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                rs.first();

                // Base info
                int exp = rs.getInt("experience");
                String username = rs.getString("username");
                UUID uuid = UUID.fromString(rs.getString("uuid"));
                Integer firstSeen = rs.getInt("first_seen");
                Integer lastSeen = rs.getInt("last_seen");
                Integer id = rs.getInt("id");
                rs.close();

                // Meta Data
                stmt = conn.prepareStatement("SELECT * FROM `site_player_meta` WHERE uuid=?");
                stmt.setString(1,u.toString());
                rs = stmt.executeQuery();

                HashMap<String,String> meta = new HashMap<String, String>();
                while (rs.next()) {
                    meta.put(rs.getString("meta_key"),rs.getString("meta_val"));
                }
                Profile profile = new Profile(this,username,uuid,exp,firstSeen,lastSeen,meta);
                rs.close();
                return profile;
            } else {
                rs.close();
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    // If a player doesnt exist.
    // This does NOT save the profile.
    public Profile createProfile(Player p) {
        int curStamp = (int) new java.util.Date().getTime();
        return new Profile(this,p.getName(),p.getUniqueId(),0,curStamp,curStamp,null);
    }

    // Saves it to the database, creating them if they dont already exist.
    public void saveProfile(Profile p) {
        Connection conn = databaseManager.getConnection();
        if (conn == null) {
            parent.getLogger().warning("Error saving profile for "+p.getUsername()+"! No database connection!");
            return;
        }
        PreparedStatement stmt;
        try {
            stmt = conn.prepareStatement("SELECT * FROM `site_players` WHERE uuid = ?");
            stmt.setString(1,p.getUniqueId().toString());
            ResultSet rs = stmt.executeQuery();

            if (rs.first()) {
                // Update existing profile
                rs.close();

                stmt = conn.prepareStatement("UPDATE `site_players` SET username=?, experience=?, first_seen=?, last_seen=? WHERE uuid=?");
                stmt.setString(1,p.getUsername());
                stmt.setInt(2,p.getExperience());
                stmt.setInt(3,p.getFirstSeen());
                stmt.setInt(4,p.getLastSeen());
                stmt.setString(5,p.getUniqueId().toString());
                stmt.execute();
            } else {

                // They dont exist, create them.
                stmt = conn.prepareStatement("INSERT INTO `site_players` (username,uuid,experience,first_seen,last_seen) VALUES(?,?,?,?,?)");
                stmt.setString(1, p.getUsername());
                stmt.setString(2, p.getUniqueId().toString());
                stmt.setInt(3, p.getExperience());
                stmt.setInt(4, p.getFirstSeen());
                stmt.setInt(5, p.getLastSeen());
                stmt.execute();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // Achievement Management
        List<PhoenixAchievement> newAchievements = p.getAchievements();
        if (newAchievements.size() > 0) {
            try {
                List<PhoenixAchievement> currentAchievements = this.parent.getAchievementManager().getAchievements(p.getUniqueId());
                for (PhoenixAchievement achv : newAchievements) {
                    stmt = conn.prepareStatement("SELECT * FROM `site_achievements_gained` WHERE uuid=? AND achievement=?");
                    stmt.setString(1,p.getUniqueId().toString());
                    stmt.setInt(2,achv.getId());
                    ResultSet rs = stmt.executeQuery();
                    if (!rs.next()) {
                        // Doesn't exist, we can insert it into the db.
                        stmt = conn.prepareStatement("INSERT INTO `site_achievements_gained` (uuid,achievement,stamp) VALUES(?,?,?)");
                        stmt.setString(1, p.getUniqueId().toString());
                        stmt.setInt(2, achv.getId());
                        stmt.setInt(3, (int) Math.floor(new java.util.Date().getTime()/1000));
                        stmt.execute();
                    }
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        // Meta Data
        try {
            HashMap<String,String> meta = p.getMeta().getContents();
            for (String mkey : meta.keySet()) {
                stmt = conn.prepareStatement("SELECT * FROM `site_player_meta` WHERE uuid=? AND meta_key=?");
                stmt.setString(1,p.getUniqueId().toString());
                stmt.setString(2,mkey);
                ResultSet rs = stmt.executeQuery();
                if (rs.next()) {
                    // Exists, update.
                    Integer id = rs.getInt("id");
                    stmt = conn.prepareStatement("UPDATE `site_player_meta` SET meta_key=?, meta_val=?, update_stamp=? WHERE id=?");
                    stmt.setString(1,mkey);
                    stmt.setString(2,meta.get(mkey));
                    stmt.setInt(3,(int) new Date().getTime());
                    stmt.setInt(4,id);
                    stmt.execute();
                } else {
                    // Doesn't exist, create.
                    stmt = conn.prepareStatement("INSERT INTO `site_player_meta` (uuid,meta_key,meta_val,update_stamp) VALUES(?,?,?,?);");
                    stmt.setString(1,p.getUniqueId().toString());
                    stmt.setString(2,mkey);
                    stmt.setString(3,meta.get(mkey));
                    stmt.setInt(4,(int) new Date().getTime());
                    stmt.execute();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
