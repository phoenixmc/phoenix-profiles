package com.infermc.phoenix.profiles;

import com.infermc.phoenix.achievements.PhoenixAchievement;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Profile {
    protected ProfileManager manager;
    private Integer exp;
    private String username;
    private UUID uuid;
    private Integer firstSeen;
    private Integer lastSeen;
    private ProfileMeta meta;
    private List<PhoenixAchievement> achievements;

    public Profile(ProfileManager pm, String uname, UUID uuid, Integer exp, Integer firstSeen, Integer lastSeen, HashMap<String,String> meta) {
        this.manager = pm;
        this.exp = exp;
        this.username = uname;
        this.uuid = uuid;
        this.firstSeen = firstSeen;
        this.lastSeen = lastSeen;
        this.achievements = this.manager.parent.getAchievementManager().getAchievements(uuid);
        if (meta != null) {
            this.meta = new ProfileMeta(this,meta);
        } else {
            this.meta = new ProfileMeta(this);
        }
    }

    // Getters
    public Integer getExperience() {
        return this.exp;
    }
    public String getUsername() {
        return this.username;
    }
    public UUID getUniqueId() {
        return this.uuid;
    }
    public Integer getFirstSeen() {
        return this.firstSeen;
    }
    public Integer getLastSeen() {
        return this.lastSeen;
    }

    public ProfileMeta getMeta() {
        return this.meta;
    }

    // Setters
    public void setExperience(Integer exp) {
        if (exp < 0) exp = 0;
        this.exp = exp;
        this.manager.saveProfile(this);
    }
    public void setUsername(String username) {
        this.username = username;
        this.manager.saveProfile(this);
    }
    public void setUniqueId(UUID u) {
        this.uuid = u;
        this.manager.saveProfile(this);
    }
    public void setFirstSeen(Integer seen) {
        this.firstSeen = seen;
        this.manager.saveProfile(this);
    }
    public void setLastSeen(Integer seen) {
        this.lastSeen = seen;
        this.manager.saveProfile(this);
    }

    // Extra methods
    public Integer getLevel() {
        return manager.util.getLevel(this.exp);
    }
    public Integer getLegendLevel() {
        return manager.util.getLegendLevel(this.exp);
    }

    // Use to activate the level up and multipliers.
    public void giveExperience(Integer amount) {
        Player player = getPlayer();

        Integer newExp = this.exp+amount;

        // Round the exp to a whole number!
        newExp = Math.round(newExp);

        Integer curLevel = getLevel();
        Integer newLevel = manager.util.getLevel(newExp);

        Integer curLegLevel = getLegendLevel();
        Integer newLegLevel = manager.util.getLegendLevel(newExp);

        // Normal Levelling
        if (newLevel > curLevel) {
            //double cash = (newLevel * 1000) / 4;
            double cash = Math.floor( (newLevel * 500) / 3 );
            manager.economy.depositPlayer(getOfflinePlayer(), cash);
            if (player != null) {
                player.sendTitle(ChatColor.GOLD + "" + ChatColor.BOLD + "Level up!", ChatColor.GREEN + "Level " + newLevel);
                player.sendMessage(ChatColor.GREEN + "You leveled up from " + curLevel + " to " + newLevel + "!");
                player.sendMessage(ChatColor.GREEN + "You leveled up and were rewarded with $" + cash);
            }

        }

        // Legendary Levelling
        if (newLegLevel > curLegLevel) {
            if (player != null) {
                player.sendTitle(ChatColor.GOLD + "" + ChatColor.BOLD + "Legendary Level up!", ChatColor.GREEN + "Legendary Level " + newLevel);
                player.sendMessage(ChatColor.GREEN + "You leveled up from " + curLevel + " to " + newLevel + "!");
            }
        }
        //getLogger().info("Cur level "+curLevel+":"+curExp+" new level "+newLevel+":"+newExp);
        setExperience(newExp);

        // Handle any possible achievements
        PhoenixAchievement achievement = manager.parent.getAchievementManager().getAchievement("level_"+newLevel);
        if (achievement != null && !hasAchievement(achievement)) giveAchievement(achievement);

        achievement = manager.parent.getAchievementManager().getAchievement("legend_"+newLegLevel);
        if (achievement != null && !hasAchievement(achievement)) giveAchievement(achievement);
    }

    public void takeExperience(Integer amount) {
        Integer cexp = this.exp;
        cexp -= amount;
        if (cexp < 0) cexp = amount;
        this.exp = cexp;
        this.manager.saveProfile(this);
    }

    // Online?
    public boolean isOnline() {
        return getPlayer() != null;
    }
    public Player getPlayer() {
        Player p = manager.parent.getServer().getPlayer(this.uuid);
        return p;
    }
    public OfflinePlayer getOfflinePlayer() {
        OfflinePlayer p = manager.parent.getServer().getOfflinePlayer(this.uuid);
        return p;
    }

    // Shows a title to the player displaying that they got Exp
    public void displayExperienceTitle(String title,Integer exp,boolean withMultiplier) {
        Player p = getPlayer();
        if (p == null) return;

        Double multiplier = manager.parent.getMultiplier();
        ProfileUtil util = manager.parent.util;

        if (withMultiplier && multiplier > 0) {
            p.sendTitle(util.colourText("&6"+title),util.colourText("&a+" + exp + " &aExperience &6(+" + (int) (manager.parent.getMultiplier() * 100) + "% Multiplier)"));
        } else {
            p.sendTitle(util.colourText("&6"+title), util.colourText("&a+" + exp + " Experience"));
        }
        p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP,10,1);
    }

    // Achievements
    public List<PhoenixAchievement> getAchievements() {
        return this.achievements;
    }

    public void giveAchievement(PhoenixAchievement achievement) {
        // If we already have it abort!
        if (this.achievements.contains(achievement)) return;
        this.achievements.add(achievement);
        this.manager.saveProfile(this);

        this.giveExperience(manager.parent.addMultiplier(achievement.getExpgain()));
        Player p = getPlayer();
        if (p != null) {
            this.displayExperienceTitle(achievement.getTitle(),achievement.getExpgain(),true);
        }
    }

    public boolean hasAchievement(PhoenixAchievement achievement) {
        for (PhoenixAchievement a : this.achievements) {
            if (a.getId() == achievement.getId()) return true;
        }
        return false;
    }
}
