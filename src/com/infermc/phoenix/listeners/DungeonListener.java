package com.infermc.phoenix.listeners;

import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.achievements.PhoenixAchievement;
import com.infermc.phoenix.profiles.Profile;
import com.infermc.phoenixrpg.dungeon.Ruin;
import com.infermc.phoenixrpg.dungeon.RuinEnterEvent;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.managers.RegionManager;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.Random;

public class DungeonListener implements Listener {
    private PhoenixProfiles parent;
    private WorldGuardPlugin wg;

    public DungeonListener(PhoenixProfiles parent) {
        this.parent = parent;
        wg = (WorldGuardPlugin) parent.getServer().getPluginManager().getPlugin("WorldGuard");
    }


    // Give the player exp
    @EventHandler
    public void onRuinEnter(RuinEnterEvent ev) {
        Player player = ev.getPlayer();
        ProtectedRegion region = ev.getRegion();
        Ruin ruin = ev.getRuin();

        String ruinType = ruin.getName().substring(ruin.getName().lastIndexOf("_")+1, ruin.getName().length());
        ruinType = ruinType.substring(0, 1).toUpperCase() + ruinType.substring(1).toLowerCase();

        Integer baseExp = 25;
        Integer minExp = 0;
        Integer maxExp = 0;

        if (ev.getRuin() != null) {
            baseExp = ruin.getBaseExp();
            minExp = ruin.getMinExp();
            maxExp = ruin.getMaxExp();
        }

        Integer finalExp = baseExp;

        // Add the random amount of exp to base.
        if (maxExp > 0) finalExp += new Random().nextInt(maxExp - minExp + 1) + minExp;

        // Do some multiplier magic.
        if (finalExp > 0) finalExp = parent.addMultiplier(finalExp);

        // Give the Exp
        Profile profile = parent.getProfileManager().getProfile(player);
        if (profile != null) {
            profile.displayExperienceTitle(ruinType + " ruin Discovered!", finalExp, true);
            profile.giveExperience(finalExp);

            Integer ruinsFound = 0;
            if (profile.getMeta().getInt("ruins_found") != null) {
                parent.getLogger().info("ruins_found doesn't exist.");
                ruinsFound = profile.getMeta().getInt("ruins_found");
            }
            parent.getLogger().info("Ruins found: "+ruinsFound);
            ruinsFound += 1;
            profile.getMeta().set("ruins_found",ruinsFound.toString());
            parent.getLogger().info("Ruins found now: "+ruinsFound);

            if (ruinsFound == 1) {
                // First ruin!
                PhoenixAchievement achv = parent.getAchievementManager().getAchievement("ruin_first");
                if (achv != null) profile.giveAchievement(achv);
            }
            if (ruinsFound == 100) {
                // First ruin!
                PhoenixAchievement achv = parent.getAchievementManager().getAchievement("like_smophers");
                if (achv != null) profile.giveAchievement(achv);
            }

            // Sneaky code to add an achievement for any amount of ruins >_>
            PhoenixAchievement achv = parent.getAchievementManager().getAchievement("ruins_"+ruinsFound);
            if (achv != null) profile.giveAchievement(achv);
        }

        // Remove the region
        RegionManager rM = wg.getRegionManager(player.getWorld());
        rM.removeRegion(region.getId());
    }
}