package com.infermc.phoenix.listeners;

import com.gamingmesh.jobs.api.JobsLevelUpEvent;
import com.gamingmesh.jobs.container.JobsPlayer;
import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.profiles.Profile;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class JobsListener implements Listener {
    private PhoenixProfiles parent;

    public JobsListener(PhoenixProfiles p) {
        this.parent = p;
    }

    @EventHandler
    public void onLevelUp(JobsLevelUpEvent ev) {
        JobsPlayer jPlayer = ev.getPlayer();
        Player player = jPlayer.getPlayer();
        Integer jobLevel = ev.getLevel();

        Integer expGain = jobLevel*5;
        expGain += expGain/2;

        Integer expGainMultiplier = this.parent.addMultiplier(expGain);

        Profile profile = parent.getProfileManager().getProfile(player);
        if (profile != null) {
            profile.displayExperienceTitle(ev.getJobName()+" Level Up!", expGainMultiplier, true);
            profile.giveExperience(expGainMultiplier);
        }
    }
}
