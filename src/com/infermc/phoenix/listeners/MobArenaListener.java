package com.infermc.phoenix.listeners;

import com.garbagemule.MobArena.events.ArenaCompleteEvent;
import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.achievements.PhoenixAchievement;
import com.infermc.phoenix.profiles.Profile;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.Set;

public class MobArenaListener implements Listener {
    private PhoenixProfiles parent;

    public MobArenaListener(PhoenixProfiles parent) {
        this.parent = parent;
    }

    @EventHandler
    public void gameEnd(ArenaCompleteEvent ev) {
        final Set<Player> survivors = ev.getSurvivors();
        parent.getServer().getScheduler().scheduleSyncDelayedTask(parent, new Runnable() {
            @Override
            public void run() {
                for (Player p : survivors) {
                    Profile profile = parent.getProfileManager().getProfile(p);
                    if (profile != null) {

                        // Mob Arena Wins
                        Integer wins = profile.getMeta().getInt("ma_wins");
                        if (wins == null) wins = 0;
                        wins++;
                        profile.getMeta().set("ma_wins",wins.toString());

                        PhoenixAchievement achievement = parent.getAchievementManager().getAchievement("ma_"+wins);
                        if (achievement != null) {
                            if (!profile.hasAchievement(achievement)) profile.giveAchievement(achievement);
                        }
                    }

                    // Give item
                    p.sendMessage(ChatColor.GREEN+"You won MobArena! Well done! Have a MobArena Token, you may cash it in at the arena NPC's!");
                    parent.getServer().dispatchCommand(parent.getServer().getConsoleSender(),"rpg giveitem "+p.getName()+" mobarena_token");
                }
            }
        },40L);
    }
}
