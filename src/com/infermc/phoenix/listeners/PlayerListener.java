package com.infermc.phoenix.listeners;

import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.achievements.PhoenixAchievement;
import com.infermc.phoenix.profiles.Profile;
import org.bukkit.Achievement;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.entity.ChestedHorse;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;

import java.util.UUID;

public class PlayerListener implements Listener {
    private PhoenixProfiles parent;

    public PlayerListener(PhoenixProfiles parent) {
        this.parent = parent;
    }

    @EventHandler
    public void onInteract(PlayerInteractEvent ev) {
        if (ev.getClickedBlock() != null) {
            if (ev.getClickedBlock().getType().equals(Material.CAKE_BLOCK)) {
                if (ev.getPlayer().getFoodLevel() < 20) {
                    Profile profile = parent.getProfileManager().getProfile(ev.getPlayer());
                    if (profile != null) {
                        PhoenixAchievement achievement = parent.getAchievementManager().getAchievement("cake_lie");
                        if (achievement != null) {
                            if (!profile.hasAchievement(achievement)) profile.giveAchievement(achievement);
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onTame(EntityTameEvent ev) {
        Entity entity = ev.getEntity();
        AnimalTamer tamer = ev.getOwner();

        if (entity instanceof ChestedHorse) {
            // Is a llama!
            UUID uuid = tamer.getUniqueId();
            Profile profile = parent.getProfileManager().getProfile(uuid);
            if (profile != null) {
                PhoenixAchievement achievement = parent.getAchievementManager().getAchievement("tame_llama");
                if (!profile.hasAchievement(achievement)) profile.giveAchievement(achievement);
            }
        }
    }
}
