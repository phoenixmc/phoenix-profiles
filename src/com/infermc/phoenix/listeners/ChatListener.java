package com.infermc.phoenix.listeners;

import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.profiles.Profile;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ChatListener implements Listener {
    private PhoenixProfiles parent;
    public ChatListener(PhoenixProfiles parent) {
        this.parent = parent;
    }

    @EventHandler
    public void onChat(AsyncPlayerChatEvent ev) {
        String format = ev.getFormat();

        Integer exp = 0;
        Integer level = 0;
        Integer legLevel = 0;
        String label = "";

        Profile profile = parent.getProfileManager().getProfile(ev.getPlayer());
        if (profile != null) {
            exp = profile.getExperience();
            level = profile.getLevel();
            legLevel = profile.getLegendLevel();
        }

        if (label == null) label = "";
        format = format.replaceAll("(?i)\\[label\\]",label);

        String level_tag = ChatColor.AQUA+"✦"+level.toString()+ChatColor.RESET;
        format = format.replaceAll("(?i)\\[level\\]",level_tag);

        String legend_tag = "";
        if (legLevel > 0) {
            legend_tag = ChatColor.GOLD+"✪"+legLevel+""+ChatColor.RESET;
        }
        format = format.replaceAll("(?i)\\[legend_level\\]", legend_tag);
        format = format.replaceAll("(?i)\\[experience\\]",exp.toString());

        // Standard Levelling tag
        String full_tag = level_tag;
        if (legend_tag != "") full_tag = legend_tag;
        format = format.replaceAll("(?i)\\[full_tag\\]",full_tag);

        ev.setFormat(format);
    }
}
