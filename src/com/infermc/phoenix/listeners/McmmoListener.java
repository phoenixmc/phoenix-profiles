package com.infermc.phoenix.listeners;

import com.gmail.nossr50.datatypes.skills.SkillType;
import com.gmail.nossr50.events.experience.McMMOPlayerLevelUpEvent;
import com.gmail.nossr50.events.party.McMMOPartyLevelUpEvent;
import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.profiles.Profile;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.util.List;

public class McmmoListener implements Listener {
    private PhoenixProfiles parent;

    public McmmoListener(PhoenixProfiles p) {
        this.parent = p;
    }

    @EventHandler
    public void onLevelUp(McMMOPlayerLevelUpEvent ev) {
        Player player = ev.getPlayer();
        Integer level = ev.getSkillLevel();

        Integer expGain = 0;
        if (ev.getSkill() == SkillType.ACROBATICS) {
            if (level > 100) {
                expGain = level / 7;
            } else if (level > 500) {
                expGain = level / 5;
            } else {
                expGain = level / 3;
            }
        } else {
            expGain = (int) Math.floor( (level*2)/3.056 );
        }

        Integer expGainMultiplier = this.parent.addMultiplier(expGain);

        Profile profile = parent.getProfileManager().getProfile(player);
        if (profile != null) {
            profile.displayExperienceTitle(ev.getSkill().getName()+" Level Up!", expGainMultiplier, true);
            profile.giveExperience(expGainMultiplier);
        }
    }

    @EventHandler
    public void onPartyLevelUp(McMMOPartyLevelUpEvent ev) {
        List<Player> players = ev.getParty().getOnlineMembers();
        Integer level = ev.getLevelsChanged();

        Integer expGain = (int) Math.floor( (level*3)/5.6435 );

        Integer expGainMultiplier = this.parent.addMultiplier(expGain);
        for (Player player : players) {
            Profile profile = parent.getProfileManager().getProfile(player);
            if (profile != null) {
                profile.displayExperienceTitle("Party Level Up!", expGainMultiplier, true);
                profile.giveExperience(expGainMultiplier);
            }
        }
    }

}
