package com.infermc.phoenix.listeners;

import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.achievements.PhoenixAchievement;
import com.infermc.phoenix.profiles.Profile;
import com.vexsoftware.votifier.model.Vote;
import com.vexsoftware.votifier.model.VotifierEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class VoteListener implements Listener {
    private PhoenixProfiles parent;

    public VoteListener(PhoenixProfiles p) {
        this.parent = p;
    }

    @EventHandler
    public void onVote(VotifierEvent ev) {
        Vote vote = ev.getVote();
        Profile profile = parent.getProfileManager().getProfile(vote.getUsername());
        if (profile != null) {
            Integer votes = profile.getMeta().getInt("votes");
            if (votes == null) votes = 0;
            votes += 1;
            profile.getMeta().set("votes",votes.toString());

            PhoenixAchievement achievement = parent.getAchievementManager().getAchievement("votes_"+votes);
            if (achievement != null) {
                if (!profile.hasAchievement(achievement)) profile.giveAchievement(achievement);
            }
        }
        storeVote(vote);
    }

    public void storeVote(Vote vote) {
        Connection conn = parent.getDatabaseManager().getConnection();
        if (conn == null) return;

        Profile profile = parent.getProfileManager().getProfile(vote.getUsername());

        try {
            PreparedStatement stmt = conn.prepareStatement("INSERT INTO `site_votes` (uuid,service,address,stamp) VALUES(?,?,?,?);");
            if (profile == null) {
                stmt.setString(1, vote.getUsername());
            } else {
                stmt.setString(1, profile.getUniqueId().toString());
            }
            stmt.setString(2,vote.getServiceName());
            stmt.setString(3,vote.getAddress());
            stmt.setInt(4,parent.util.getTime());
            stmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
