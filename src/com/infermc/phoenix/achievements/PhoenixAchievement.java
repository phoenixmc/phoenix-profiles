package com.infermc.phoenix.achievements;

// Represents an Achievement for Phoenix
// SETTERS DO NOT UPDATE THE DATABASE!!!
public class PhoenixAchievement {
    int id;
    String slug;
    String title;
    String description;
    int expgain;

    public PhoenixAchievement(int id) {
        this.id = id;
    }

    // Setters
    public void setSlug(String slug) {
        this.slug = slug;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public void setDescription(String desc) {
        this.description = desc;
    }
    public void setExpgain(int expgain) {
        this.expgain = expgain;
    }

    // Getters
    public int getId() {
        return this.id;
    }
    public String getSlug() {
        return this.slug;
    }
    public String getTitle() {
        return this.title;
    }
    public String getDescription() {
        return this.description;
    }
    public int getExpgain() {
        return this.expgain;
    }
}
