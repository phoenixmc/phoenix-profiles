package com.infermc.phoenix.achievements;

import com.infermc.phoenix.DatabaseManager;
import com.infermc.phoenix.PhoenixProfiles;
import com.infermc.phoenix.profiles.ProfileUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class AchievementManager {
    protected PhoenixProfiles parent;
    protected ProfileUtil util;
    protected DatabaseManager databaseManager;

    public AchievementManager(PhoenixProfiles parent, DatabaseManager connMgr) {
        this.parent = parent;
        this.util = parent.util;
        this.databaseManager = connMgr;
    }

    public List<PhoenixAchievement> getAchievements() {
        List<PhoenixAchievement> achievements = new ArrayList<PhoenixAchievement>();
        try {
            PreparedStatement stmt = this.databaseManager.getConnection().prepareStatement("SELECT * FROM `site_achievements`");
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                PhoenixAchievement achv = new PhoenixAchievement(rs.getInt("id"));
                achv.setSlug(rs.getString("slug"));
                achv.setDescription(rs.getString("description"));
                achv.setTitle(rs.getString("title"));
                achv.setExpgain(rs.getInt("expgain"));
                achievements.add(achv);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return achievements;
    }
    public List<PhoenixAchievement> getAchievements(UUID uuid) {
        List<PhoenixAchievement> achievements = new ArrayList<PhoenixAchievement>();
        try {
            PreparedStatement stmt = this.databaseManager.getConnection().prepareStatement("SELECT * FROM `site_achievements_gained` WHERE uuid=?");
            stmt.setString(1,uuid.toString());
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                PhoenixAchievement achv = this.getAchievement(rs.getInt("achievement"));
                if (achv != null) {
                    achievements.add(achv);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return achievements;
    }
    public PhoenixAchievement getAchievement(int id) {
        Connection conn = this.databaseManager.getConnection();
        if (conn == null) return null;
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM `site_achievements` WHERE id=?");
            stmt.setInt(1,id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                rs.first();
                PhoenixAchievement achv = new PhoenixAchievement(rs.getInt("id"));
                achv.setSlug(rs.getString("slug"));
                achv.setDescription(rs.getString("description"));
                achv.setTitle(rs.getString("title"));
                achv.setExpgain(rs.getInt("expgain"));
                return achv;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    public PhoenixAchievement getAchievement(String slug) {
        Connection conn = this.databaseManager.getConnection();
        if (conn == null) return null;
        PreparedStatement stmt = null;
        try {
            stmt = conn.prepareStatement("SELECT * FROM `site_achievements` WHERE slug=?");
            stmt.setString(1,slug);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                rs.first();
                PhoenixAchievement achv = new PhoenixAchievement(rs.getInt("id"));
                achv.setSlug(rs.getString("slug"));
                achv.setDescription(rs.getString("description"));
                achv.setTitle(rs.getString("title"));
                achv.setExpgain(rs.getInt("expgain"));
                return achv;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
