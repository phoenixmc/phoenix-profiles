package com.infermc.phoenix;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseManager {
    private Connection connection;
    private PhoenixProfiles parent;
    private String connectionString;

    public DatabaseManager(PhoenixProfiles parent,String connString) {
        this.connectionString = connString;
        this.parent = parent;
        this.connection = getConnection();
    }

    public Connection getConnection() {
        try {
            if (connection == null || connection.isClosed()) {
                Connection conn = createConnection();
                this.connection = conn;
                return conn;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            this.connection = null;
        }
        return connection;
    }

    private Connection createConnection() {
        Connection conn;
        try {
            conn = DriverManager.getConnection(this.connectionString);
            parent.getLogger().info("Connected to database!");
            return conn;
        } catch (SQLException e) {
            //e.printStackTrace();
            parent.getLogger().warning("Error Connecting to MySQL: "+e.getMessage());
        }
        return null;
    }
}
